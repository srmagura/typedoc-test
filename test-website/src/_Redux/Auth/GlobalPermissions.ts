export interface GlobalPermissions {
    canViewOrders: boolean
    canManageOrders: boolean
}
