export enum PermissionName {
    CanViewOrders = 'CanViewOrders',
    CanManageOrders = 'CanManageOrders',
    CanManageCustomer = 'CanManageCustomer',
    CanManageVendor = 'CanManageVendor',
    CanManageCustomerVendorMap = 'CanManageCustomerVendorMap',
}
