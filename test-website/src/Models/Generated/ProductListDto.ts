// Auto-generated by ITI.TypeScriptDtoGenerator - do not edit
import { ProductDto } from './ProductDto'

export interface ProductListDto {
    products: ProductDto[]
    totalFilteredCount: number
}
