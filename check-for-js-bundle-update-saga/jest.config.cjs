﻿const common = require('@interface-technologies/jest-config')

module.exports = {
    ...common,
    timers: 'real',
}
