﻿namespace Dto
{
    public enum ErrorDtoType
    {
        InternalServerError,
        InvalidLogin,
        NotAuthorized,
        UserDoesNotExist
    }
}